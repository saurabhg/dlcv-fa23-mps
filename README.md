### CS 444: Deep Learning for Computer Vision (Fall 2023)

1. [MP1](./mp1). Due Sep 19 2023, 11:59:59PM

2. [MP2](./mp2). Due Oct 5 2023, 11:59:59PM

3. [MP3](./mp3). Due Oct 26 2023, 11:59:59PM

4. [MP4](./mp4). Due Nov 14 2023, 11:59:59PM

5. [MP5](./mp5). Due Nov 30 2023, 11:59:59PM
